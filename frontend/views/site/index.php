<?php
use yii\bootstrap\Nav;
$this->title = 'Online Game Portal';
/* @var $this yii\web\View */

?>
<div class="site-index">

<?php 
if (!Yii::$app->user->isGuest)
{
?>
    <div>
        <p><a class="btn btn-lg btn-success" href="<?php echo \Yii::$app->urlManager->createUrl(['site/new-topic']) ?>"><span class="glyphicon glyphicon-file"></span> New &raquo;</a></p>
    </div>
<?php
}
?>

    <div class="body-content">

        <div class="row">
			<div class="col-lg-9">
				<?php				
				foreach($posts as $post){
					echo '<div>';
					echo '<a href="'.\Yii::$app->urlManager->createUrl(['site/details', 'id' => $post->id]).'"><h2>'.$post->title.'</h2></a>';
					echo '<p>'.substr($post->content,0,300).'... <a href="'.\Yii::$app->urlManager->createUrl(['site/details', 'id' => $post->id]).'">readmore &raquo;</a></p>';
					
					if($post->image)
					{
						echo '<img src="images/'.$post->image.'" height="200">';
					}
					echo '<p>';
					echo '<p><small>Posted by '.$post->user->username.' at '.date('F j, Y, g:i a',$post->created_at).'</small></p>';
										
					if (!Yii::$app->user->isGuest)
					{
						if(Yii::$app->user->identity->id === $post->user_id)
						{
							echo '<a class="btn btn-default" href="'.\Yii::$app->urlManager->createUrl(['site/new-topic', 'id' => $post->id]).'"><span class="glyphicon glyphicon-edit"></span> Edit &raquo;</a>';
						}
					}
					
					echo '</p>';
					echo '</div>';
				}
				?>                
            </div>
            <div class="col-lg-3">
                <h2>Category</h2>
				<?php				
				$items=[];	
				foreach($categories as $category){
					$items[]=['label' => $category->name , 'url' => \Yii::$app->urlManager->createUrl(['site/category-list', 'id' => $category->id])];
				}
				echo Nav::widget([
					'items' => $items,
				]);
				?>                
            </div>
        </div>

    </div>
</div>
