<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;

$this->title = 'Create/Edit';
/* @var $this yii\web\View */
/* @var $model common\models\Post */
/* @var $form ActiveForm */
?>
<h1><?= Html::encode($this->title) ?></h1>
<?php
$this->params['breadcrumbs'][] = $this->title;
?>
<p>Please fill out the form</p>

<div class="site-newTopicForm">
	
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <?= $form->field($model, 'category_id')->dropDownList(
            ArrayHelper::map($categories, 'id', 'name')
            ) ?>
        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'content')->widget(TinyMce::className(), [
    'options' => ['rows' => 6],
    'language' => 'en_GB',
    'clientOptions' => [
        'plugins' => [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    ]
]) ?>
        <?php
		if($model->image)
		{
			echo '<img src="images/'.$model->image.'" height="320">'; 
		}
		?>
		<?= $form->field($model, 'image')->fileInput() ?>

    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-newTopicForm -->
