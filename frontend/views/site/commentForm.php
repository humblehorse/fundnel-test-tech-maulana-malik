<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Comment */
/* @var $form ActiveForm */
?>
<div class="site-commentForm">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'email')->textinput(['style'=>'width:20%;']) ?>
        <?= $form->field($model, 'url')->textinput(['style'=>'width:50%;']) ?>
		<?= $form->field($model, 'content')->textarea(['rows'=>3]) ?>
    
        <div class="form-group">
			<?php
			if (!Yii::$app->user->isGuest)
			{
				echo Html::submitButton('Say It!', ['class' => 'btn btn-primary']);
			}
			else
			{
				echo Html::submitButton('Please Login to Comment', ['class' => 'btn btn-primary', 'disabled'=>true]);
			}
			?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-commentForm -->
