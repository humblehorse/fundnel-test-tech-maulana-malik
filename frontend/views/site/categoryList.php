<?php
use yii\bootstrap\Nav;
use yii\helpers\Html;

$this->title = 'Category: '.$cat_name;
/* @var $this yii\web\View */

?>

<?php
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-index">
<!--
    <div>
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>
-->
    <div class="body-content">

        <div class="row">
			<div class="col-lg-9">
				<?php				
				foreach($posts as $post){
					echo '<div>';
					echo '<a href="'.\Yii::$app->urlManager->createUrl(['site/details', 'id' => $post->id]).'"><h2>'.$post->title.'</h2></a>';
					echo '<p>'.substr($post->content,0,300).'... <a href="'.\Yii::$app->urlManager->createUrl(['site/details', 'id' => $post->id]).'">readmore &raquo;</a></p>';
					if($post->image)
					{
						echo '<img src="images/'.$post->image.'" height="200">';
					}
					echo '<p><small>Posted by '.$post->user->username.' at '.date('F j, Y, g:i a',$post->created_at).'</small></p>';
					echo '</div>';
				}
				?>                
            </div>
            <div class="col-lg-3">
                <h2>Category</h2>
				<?php				
				$items=[];	
				foreach($categories as $category){
					$items[]=['label' => $category->name , 'url' => \Yii::$app->urlManager->createUrl(['site/category-list', 'id' => $category->id])];
				}
				echo Nav::widget([
					'items' => $items,
				]);
				?>                
            </div>
        </div>

    </div>
</div>
