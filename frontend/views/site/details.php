<?php
use yii\bootstrap\Nav;
use yii\helper\HTML;

$this->title = $post->title;
/* @var $this yii\web\View */


?>

<?php
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-index">
<!--
    <div>
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>
-->
    <div class="body-content">

        <div class="row">
			<div class="col-lg-9">
				<?php				
				// foreach($post as $post){
					echo '<div>';
					echo '<h2>'.$post->title.'</h2>';
					echo '<p><small>Posted by '.$post->user->username.' at '.date('F j, Y, g:i a',$post->created_at).'</small></p>';
					if($post->image)
					{
						echo '<img src="images/'.$post->image.'" height="420">' ;
					}
					echo '<p>'.$post->content.'</p>';
					// echo '<p><a class="btn btn-default" href="'.\Yii::$app->urlManager->createUrl(['site/details', 'id' => $post->id]).'">readmore &raquo;</a></p>';
					echo '</div>';
				// }
				
				echo '<br><h4>Comment <span class="glyphicon glyphicon-comment"></span></h4>';
				
				foreach($comments as $comment)
				{
					echo "<div style='border-bottom:1px solid #ddd; padding:5px;margin:5px;'>";
					echo "<p><small>
					  Comment by ".$comment->user->username." at ".date("F j, Y, g:i a",$comment->created_at).
					  "</small></p>";
					echo $comment->content;
					echo "</div>";
				}
				
				if(empty($comments))
				{
					echo "<div padding:5px;margin:5px;'>";
					echo 'No Comment In This Article Yet!';
					echo "</div>";
				}
				
				echo '<div class="clearfix"></div><br>';
				
				echo $this->render('commentForm', [
					'model' => $model,
				]);
				?>                
            </div>
            <div class="col-lg-3">
                <h2>Category</h2>
				<?php				
				$items=[];	
				foreach($categories as $category){
					$items[]=['label' => $category->name , 'url' => \Yii::$app->urlManager->createUrl(['site/category-list', 'id' => $category->id])];
				}
				echo Nav::widget([
					'items' => $items,
				]);
				?>                
            </div>
        </div>

    </div>
</div>
