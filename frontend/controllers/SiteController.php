<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
	{
		$posts = \common\models\Post::find()
		->where(['status' => 1])
		->orderBy('id DESC')
		->all();
		
		$categories = \common\models\Category::find()
		->orderBy('name ASC')
		->all();
		
		return $this->render('index', [
			'posts' => $posts,
			'categories' => $categories,
		]);
	}

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
	
	public function actionDetails($id)
	{
		$post = \common\models\Post::find()
		->where(['status' => 1, 'id' => $id])
		->one();
	
		$categories = \common\models\Category::find()
		->orderBy('name ASC')
		->all();
		
		$comments = \common\models\Comment::find()
        ->where(['status' => 1,'post_id'=>$id])
        ->orderBy('id DESC')
        ->all();
		
		$model = new \common\models\Comment();
		
		if ($model->load(Yii::$app->request->post())) 
		{
			$model->post_id=$id;
			$model->created_at=time();

			if (!Yii::$app->user->isGuest)
			{
				$model->user_id=Yii::$app->user->identity->id;
				$model->email=Yii::$app->user->identity->email;
				$model->status=1;
			}
			
			if ($model->validate()) 
			{
				if($model->save())
				{
					Yii::$app->session->setFlash('success', 'Comment was saved.');
					return $this->redirect(Url::current(),302);
				}
			}	
		}
	
		return $this->render('details', [
			'post' => $post,
			'categories' => $categories,
			'comments' => $comments,
			'model' => $model,
		]);
	}
	
	public function actionCategoryList($id)
	{
		$posts = \common\models\Post::find()
        ->where(['status' => 1,'category_id'=>$id])
        ->orderBy('id DESC')
        ->all();
		
		$categories = \common\models\Category::find()
		->orderBy('name ASC')
		->all();
		
		$cat_name = \common\models\Category::find()
		->where(['id'=>$id])
		->one();
		
		return $this->render('categoryList', [
			'categories' => $categories,
			'posts' => $posts,
			'cat_name' => $cat_name['name'],
		]);
	}
	
	public function actionNewTopic($id = null)
	{
		$time = time();
		$categories = \common\models\Category::find()
		->orderBy('name ASC')
		->all();
		
		if($id == null)
		{
			$model = new \common\models\Post();
			$model->created_at=$time;
		}
		else
		{
			$model = \common\models\Post::find()->where(['id' => $id])->one();;
			$model->updated_at=$time;
			$img = $model->image;
		}
		
		if ($model->load(Yii::$app->request->post())) 
		{
			$file = UploadedFile::getInstance($model, 'image');
			if($file && $file->getExtension() != 'jpg')
			{
				Yii::$app->session->setFlash('error', 'Error, Please upload jpg image only');
				return $this->redirect(Url::current(),302);
			}
			if($file)
			{
				$model->image = $time.$file->name;
			}
			else
			{
				if($id != null)
				{
					$model->image = $img;
				}
			}
	
			$model->user_id=Yii::$app->user->identity->id;
			$model->status=1;

			if ($model->validate()) 
			{
				if($model->save())
				{
					if($file)
					{
						 $file->saveAs('images/'.$time.$file->name);
					}
					Yii::$app->session->setFlash('success', 'Article was saved,');
					return $this->redirect(Url::current(['id'=>$model->id]),302);
				}
			}
		}
		
		return $this->render('newTopicForm', [
			'categories' => $categories,
			'model' => $model,
		]);
	}
}
